package com.example.demo.service;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.service.impl.StringServiceImpl;

public class StringServiceTest {
	@Autowired
	private StringServiceImpl str = new StringServiceImpl();

	
	@Test
	public void stringCount () {
		Integer actual = str.countVal("raj");
		Integer expected = 3;
		assertEquals(actual, expected);
	}
	
	@Test
	public void stringRev () {
		String actual = str.reverseString("raj");
		String expected = "jar";
		assertEquals(actual, expected);
	}
	
	@Test
	public void stringRevpat () {
		str.reverseString("raj");
		
	}
}
