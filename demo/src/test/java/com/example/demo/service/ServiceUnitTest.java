package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.service.impl.StarPatternServiceImpl;

public class ServiceUnitTest {

	@Autowired
	private StarPatternServiceImpl starPatternservice = new StarPatternServiceImpl();

	@Test
	public void testServiceStar() {
		String actual = starPatternservice.printPattern(4, "postman");
		String expected = "*\n**\n***\n";
		assertEquals(actual, expected);
	}

	@Test
	public void testServiceStarBr() {
		String actual = starPatternservice.printPattern(4, "raj");
		String expected = "*<br/>**<br/>***<br/>";
		assertEquals(actual, expected);
	}

	@Test
	public void testServiceAsh() {
		String actual = starPatternservice.numberPattern(4, "postman");
		String expected = "#\n##\n###\n";
		assertEquals(actual, expected);
	}

	@Test
	public void testServiceAshBr() {
		String actual = starPatternservice.numberPattern(4, "raj");
		String expected = "#<br/>##<br/>###<br/>";
		assertEquals(actual, expected);
	}

	@Test
	public void revPatternBr() {
		String actual = starPatternservice.revPattern(3, "raj");
		String expected = "***<br/>**<br/>*<br/>";
		assertEquals(actual, expected);
	}

	@Test
	public void revPattern() {
		String actual = starPatternservice.revPattern(3, "postman");
		String expected = "***\n**\n*\n";
		assertEquals(actual, expected);
	}

	
}
