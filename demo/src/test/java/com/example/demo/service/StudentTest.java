/*
 * package com.example.demo.service;
 * 
 * import static org.junit.jupiter.api.Assertions.*; import
 * org.junit.jupiter.api.Test; import com.example.demo.dto.StudentDto; import
 * com.example.demo.service.impl.*;
 * 
 * public class StudentTest {
 * 
 * private StudentService student = new StudentsServiceImpl();
 * 
 * // StudentDto[] stuDetails = student.getStudents();
 * 
 * @Test public void studentTestAge() { int actual = stuDetails[0].getAge(); int
 * expected = 21; assertEquals(actual, expected);
 * 
 * }
 * 
 * @Test public void studentTestname() { String actual =
 * stuDetails[1].getName(); String expected = "raj"; assertEquals(actual,
 * expected);
 * 
 * }
 * 
 * @Test public void studentTestageArray() { int[] actual = {
 * stuDetails[0].getAge(), stuDetails[1].getAge(), stuDetails[2].getAge(),
 * stuDetails[3].getAge(), stuDetails[4].getAge(), stuDetails[5].getAge(),
 * stuDetails[6].getAge(), stuDetails[7].getAge(), stuDetails[8].getAge(),
 * stuDetails[9].getAge() }; int[] expected = { 21, 22, 23,22, 23, 23, 22, 23,
 * 22, 22 }; assertArrayEquals(actual, expected);
 * 
 * }
 * 
 * @Test public void studentTestClass() { String actual =
 * stuDetails[1].getClasss(); String expected = "12th"; assertEquals(actual,
 * expected);
 * 
 * }
 * 
 * @Test public void studentTestSection() { String actual =
 * stuDetails[1].getSection(); String expected = "A"; assertEquals(actual,
 * expected);
 * 
 * }
 * 
 * @Test public void studentTamilArray() { int[] actual = {
 * stuDetails[0].getTamil(), stuDetails[1].getTamil(), stuDetails[2].getTamil(),
 * stuDetails[3].getTamil(), stuDetails[4].getTamil(), stuDetails[5].getTamil(),
 * stuDetails[6].getTamil(), stuDetails[7].getTamil(), stuDetails[8].getTamil(),
 * stuDetails[9].getTamil() }; int[] expected = { 99, 77, 77, 75, 67, 88, 90,
 * 67, 77,77 }; assertArrayEquals(actual, expected); }
 * 
 * @Test public void studentTestenglish() { int actual =
 * stuDetails[1].getEnglish(); int expected = 78; assertEquals(actual,
 * expected);
 * 
 * }
 * 
 * @Test public void studentChemistry() { int actual =
 * stuDetails[1].getChemistry(); int expected = 56; assertEquals(actual,
 * expected);
 * 
 * }
 * 
 * @Test public void studentMaths() { int actual = stuDetails[8].getMaths(); int
 * expected = 80; assertEquals(actual, expected);
 * 
 * }
 * 
 * @Test public void studentPhysics() { int actual = stuDetails[4].getPhysics();
 * int expected = 80; assertEquals(actual, expected);
 * 
 * }
 * 
 * @Test public void testname() { String actual =
 * student.getParticularStudent(2).getName(); String expected = "raj";
 * assertEquals(actual, expected); }
 * 
 * @Test public void studentPhysicsArray() { int[] actual = {
 * stuDetails[0].getPhysics(), stuDetails[1].getPhysics(),
 * stuDetails[2].getPhysics(), stuDetails[3].getPhysics(),
 * stuDetails[4].getPhysics(), stuDetails[5].getPhysics(),
 * stuDetails[6].getPhysics(), stuDetails[7].getPhysics(),
 * stuDetails[8].getPhysics(), stuDetails[9].getPhysics() }; int[] expected = {
 * 100, 90, 80, 97, 80, 50, 70, 99, 90,90 }; assertArrayEquals(actual,
 * expected);
 * 
 * }
 * 
 * }
 */