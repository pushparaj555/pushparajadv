package com.example.demo.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.example.demo.controller.StarPatternController;
import com.example.demo.service.StarPatternService;

@RunWith(MockitoJUnitRunner.class)
public class StarControllerTest {

	@InjectMocks
	private StarPatternController starPatternController;

	@Mock
	private StarPatternService starPatternService;

	@Test
	public void testPrintPattern() {
		when(starPatternService.printPattern(Mockito.any(Integer.class), Mockito.any(String.class)))
					.thenReturn("Sample");
		ResponseEntity<String> responseEntity = starPatternController.printPattern(1, "browser");
		assertEquals("Sample", responseEntity.getBody());
	}

	@Test
	public void testNumberPattern() {
		when(starPatternService
					.numberPattern(Mockito.any(Integer.class), Mockito.any(String.class)))
					.thenReturn("sample");
		ResponseEntity<String> responseEntity = starPatternController.numberPattern(1, "browser");
		assertEquals("sample", responseEntity.getBody());
	}

}



































//doNothing().when(starPatternService).printPattern(Mockito.any(Integer.class),
// Mockito.any(String.class));
// when(starPatternService.printPattern(1, "POSTMAN")).thenReturn("Sample");
