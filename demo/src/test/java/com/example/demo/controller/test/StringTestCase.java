package com.example.demo.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.controller.StringController;
import com.example.demo.dto.CharacterDto;
import com.example.demo.service.StringService;

@RunWith(MockitoJUnitRunner.class)
public class StringTestCase {

	@InjectMocks
	private StringController stringController;

	@Mock
	private StringService stringService;

	CharacterDto charDto;

	@Before
	public void init() {
		charDto = new CharacterDto();
	}

	@Test
	public void testString() {
		when(stringService.countVal(Mockito.any(String.class))).thenReturn(2);
		assertEquals(stringController.countVal("something").getBody(), 2);
	}

	@Test
	public void testrev() {
		when(stringService.reverseString(Mockito.any(String.class))).thenReturn("jar");
		assertEquals(stringController.reverseString("raj").getBody(), "jar");
	}
	

}
