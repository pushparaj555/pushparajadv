package com.example.demo.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.example.demo.controller.StudentController;
import com.example.demo.dto.StudentDto;
import com.example.demo.service.StudentService;

@RunWith(MockitoJUnitRunner.class)
public class StudentControllerTest {

	@InjectMocks
	private StudentController studentController;

	@Mock
	private StudentService studentService;

	private StudentDto[] studentDtoArr = new StudentDto[1];

	private StudentDto studentDto = new StudentDto();

//	@Test
//	public void getstudents() {
//		when(studentService.getStudents()).thenReturn(studentDtoArr);
//		assertEquals(studentController.getStudents().getBody(), studentDtoArr);
//	}

	@Test
	public void getstudentsById() {
		when(studentService.getParticularStudent(Mockito.anyInt())).thenReturn(studentDto);
		ResponseEntity<StudentDto> responseEntity = studentController.getParticularStudent(1);
		assertEquals(studentDto, responseEntity.getBody());
	}

	@Test
	public void createStudent() {
		doNothing().when(studentService).createStudent(Mockito.any(StudentDto.class));
		assertEquals(studentController.createStudent(new StudentDto()).getStatusCodeValue(), 201);
	}

	@Test
	public void putStudentTest() {
		doNothing().when(studentService)
					.updateStudent(Mockito.anyInt(), Mockito.any(StudentDto.class));
		assertEquals(studentController.updateStudent(1, studentDto).getStatusCodeValue(), 200);
	}

	@Test
	public void patchStudent() {
		doNothing().when(studentService)
					.selUpdStudent(Mockito.anyInt(), Mockito.any(StudentDto.class));
		assertEquals(studentController.selUpdStudent(1, studentDto).getStatusCodeValue(), 200);
	}

	@Test
	public void deleteStudentTest() {
		doNothing().when(studentService).deleteStudent(Mockito.anyInt());
		assertEquals(studentController.deleteStudent(7).getStatusCodeValue(), 204);
	}

	@Test
	public void deleteStudentTesting() {
		doNothing().when(studentService).deleteStudent(9);
		assertEquals(studentController.deleteStudent(9).getStatusCodeValue(), 204);
	}

}
