package com.example.demo.dto;

public class AddressDto {

	private long id;

	public long getId() {
		return id;
	}

	private String area;

	private String district;

	private int pincode;

	private int StudId;

	
	public int getStudId() {
		return StudId;
	}

	
	public void setStudId(int studId) {
		StudId = studId;
	}

	public void setId(long l) {
		this.id = l;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

}
