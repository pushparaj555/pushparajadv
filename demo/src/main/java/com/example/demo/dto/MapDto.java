package com.example.demo.dto;

public class MapDto {

	private int id;

	private String name;

	private int age;

	private String standard;

	private String section;

	public MapDto(int id, String name, int age, String standard, String section) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.standard = standard;
		this.section = section;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Override
	public String toString() {
		return "MapDto [id=" + id + ", name=" + name + ", age=" + age + ", standard=" + standard
					+ ", section=" + section + "]";
	};

}
