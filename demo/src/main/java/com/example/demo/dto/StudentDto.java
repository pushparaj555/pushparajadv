package com.example.demo.dto;

import java.util.List;

//import com.adv.training06ms.dto.AddressDto;

public class StudentDto {

	private long id;

	private String name;

	private int age;

	private String classs;

	private String section;
	
	
	private List<AddressDto> addressDtos;

	public StudentDto() {
	}

	
//public StudentDto(long id, String name, int age, String classs, String section,
//				List<AddressDto> addressDtos) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.age = age;
//		this.classs = classs;
//		this.section = section;
//		this.addressDtos = addressDtos;
//	}


	public StudentDto(long id, String name, int age, String classs, String section) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.classs = classs;
		this.section = section;
	}

	@Override
public String toString() {
	return "StudentDto [id=" + id + ", name=" + name + ", age=" + age + ", classs=" + classs
				+ ", section=" + section + ", addressDtos=" + addressDtos + "]";
}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getClasss() {
		return classs;
	}

	public void setClasss(String classs) {
		this.classs = classs;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	
	public List<AddressDto> getAddressDtos() {
		return addressDtos;
	}

	
	public void setAddressDtos(List<AddressDto> addressDtos) {
		this.addressDtos = addressDtos;
	}


	
	
	}

