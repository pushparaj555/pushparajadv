package com.example.demo.dto;

public class CharacterDto {

	private char val;

	private int count;

	public char getVal() {
		return val;
	}

	public void setVal(char val) {
		this.val = val;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
