package com.example.demo.dto;

public class CollectionDto {

	private int id;

	private String name;

	private String email;

	public CollectionDto(int id, String name, String email) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public int hashCode() {
		return email.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		CollectionDto collectionDto = (CollectionDto) obj;
		return email.equals(collectionDto.getEmail());
	}

	@Override
	public String toString() {
		return "CollectionDto [email]";
	}

}
