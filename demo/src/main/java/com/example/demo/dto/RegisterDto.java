package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Id;

public class RegisterDto {

	private String email;

	private String firstName;

	private String lastName;

	
	public String getEmail() {
		return email;
	}

	
	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getFirstName() {
		return firstName;
	}

	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
	public String getLastName() {
		return lastName;
	}

	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public LoginDto getLoginDto() {
		return loginDto;
	}

	
	public void setLoginDto(LoginDto loginDto) {
		this.loginDto = loginDto;
	}

	private LoginDto loginDto;

}
