package com.example.demo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.*;
import com.example.demo.service.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class StudentController {

	private Logger LOG = LogManager.getLogger(StudentController.class);

	@Autowired
	@Qualifier(value = "studentServiceImpl")
	private StudentService studentService;

	@GetMapping(value = "/students")
	public ResponseEntity<List<StudentDto>> getStudents() {
		List<StudentDto> studentDtos = studentService.getStudents();
		return new ResponseEntity<List<StudentDto>>(studentDtos, HttpStatus.OK);

	}
	
//	@GetMapping(value = "/students")
//	public ResponseEntity<List<StudentDto>> getStudents() {
////		List<StudentDto> studentDtos = studentService.getStudents();
//		
//		return new ResponseEntity<List<StudentDto>>(studentService.getStudents(), HttpStatus.OK);
//
//	}

	@GetMapping(value = "/students/{studentid}")
	public ResponseEntity<StudentDto>
				getParticularStudent(@PathVariable(value = "studentid") int studentId) {
		LOG.info("studentid=" + studentId);
		StudentDto studentDto = studentService.getParticularStudent(studentId);
		return new ResponseEntity<StudentDto>(studentDto, HttpStatus.OK);
	}

	@PostMapping(value = "/students")
	public ResponseEntity<Void> createStudent(@RequestBody StudentDto studentDto) {
		studentService.createStudent(studentDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping(value = "/students/{studentid}")
	public ResponseEntity<Void> updateStudent(@PathVariable(value = "studentid") int studentId,
				@RequestBody StudentDto studentDto) {
		studentService.updateStudent(studentId, studentDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@PatchMapping(value = "/students/{studentid}")
	public ResponseEntity<Void> selUpdStudent(@PathVariable(value = "studentid") int studentId,
				@RequestBody StudentDto studentDto) {
		studentService.selUpdStudent(studentId, studentDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/students/{studentid}")
	public ResponseEntity<Void> deleteStudent(@PathVariable(value = "studentid") int studentId) {
		LOG.info("studentid=" + studentId);
		studentService.deleteStudent(studentId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
