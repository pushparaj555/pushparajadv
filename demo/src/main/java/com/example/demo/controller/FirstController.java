package com.example.demo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.FirstDto;

@RestController

@CrossOrigin(origins = "http://127.0.0.1:5500")
public class FirstController {

	private Logger LOGGER = LogManager.getLogger(FirstController.class);

	@GetMapping(value = "/welcome")
	public ResponseEntity<String> helloWorld() {
		ResponseEntity<String> responseEntity = new ResponseEntity<>("Welcome to Microservice",
					HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/welcomesample")
	public ResponseEntity<Integer> helloWorld(@RequestParam(value = "a") int a,
				@RequestParam(value = "b") int b) {
		int c = a + b;
		return new ResponseEntity<>(c, HttpStatus.OK);
	}

	@GetMapping(value = "/welcome/params")
	public ResponseEntity<String> helloWorldWithParams(
				@RequestParam(value = "firstname") String firstName,
				@RequestParam(value = "lastname") String lastName) {
		{
			String message = "Hai i am " + firstName + " " + lastName;

			ResponseEntity<String> responseEntity = new ResponseEntity<>(message, HttpStatus.OK);
			return responseEntity;
		}

	}

	@GetMapping(value = "/welcome/path/{firstname}/{lastname}")
	public ResponseEntity<String> helloWorldWithPathvariables(
				@PathVariable(value = "firstname") String firstName,
				@PathVariable(value = "lastname") String lastName) {
		{
			String message = "Hai i am " + firstName + " " + lastName;

			ResponseEntity<String> responseEntity = new ResponseEntity<>(message, HttpStatus.OK);
			return responseEntity;
		}
	}

	@PostMapping(value = "/welcome")
	public ResponseEntity<Void> helloWorldWithPost(@RequestBody FirstDto firstDto) {
		String message = "Hai i am " + firstDto.getFirstName() + " " + firstDto.getLastName();
		LOGGER.info(message);
		LOGGER.warn(message);
		LOGGER.error(message);
		LOGGER.fatal(message);

		System.out.println("Message=" + message);
		ResponseEntity<Void> responseEntityy = new ResponseEntity<>(HttpStatus.CREATED);
		return responseEntityy;
	}

	@GetMapping(value = "number")
	public ResponseEntity<String> helloWorld(@RequestParam(value = "value") int value) {
		String star = "";
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				star += j + " ";
			}
			if (value == 1) {
				star += "\n";
			} else {
				star += "<br>";
			}
		}
		ResponseEntity<String> responseEntity = new ResponseEntity<>(star, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "star")
	public ResponseEntity<String> helloWorldstar(@RequestParam(value = "value") int value) {
		String star = "";
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= i; j++) {
				star += "*" + " ";
			}
			if (value == 1) {
				star += "\n";
			} else {
				star += "<br>";
			}
		}
		ResponseEntity<String> responseEntity = new ResponseEntity<>(star, HttpStatus.OK);
		return responseEntity;
	}
}
