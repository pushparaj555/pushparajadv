package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.*;

@RestController
public class ArrayController {

	@Autowired
	@Qualifier(value = "arrayServiceImpl")
	private ArrayService arrayService;

	@GetMapping(value = "/patternArr")
	public ResponseEntity<Integer[]> array() {
		Integer[] pattern = arrayService.array();
		return new ResponseEntity<>(pattern, HttpStatus.OK);
	}

	@GetMapping(value = "/arrayev")
	public ResponseEntity<Integer[]> arrayEven(@RequestParam(value = "rows") int val,
				@RequestParam(value = "rows2") int val2) {
		Integer[] pattern = arrayService.arrayEven(val, val2);
		return new ResponseEntity<>(pattern, HttpStatus.OK);
	}

}
