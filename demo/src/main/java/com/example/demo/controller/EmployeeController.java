package com.example.demo.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.*;
import com.example.demo.service.*;

@RestController
@CrossOrigin(origins = "http://127.0.0.1:5500")
public class EmployeeController {
	
	@Autowired
	@Qualifier(value = "empServiceImpl") 
	private EmployeeService employeeeService;

	@GetMapping(value = "/employee")
	public ResponseEntity<EmployeeDto[]> getEmployees() {
		return new ResponseEntity<EmployeeDto[]>(employeeeService.getEmployees(), HttpStatus.OK);

	}
	
	@GetMapping(value = "/emploId/{employeeid}")
	public ResponseEntity<EmployeeDto>
	getEmp(@PathVariable(value = "employeeid") int empId) {
		EmployeeDto employeeDto = employeeeService.getEmp(empId);
		return new ResponseEntity<EmployeeDto>(employeeDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "/postEmployee")
	public ResponseEntity<Void> createEmp(@RequestBody EmployeeDto employeeDto) {
		employeeeService.createEmp(employeeDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/postEmployeeadd")
	public ResponseEntity<Void> setEmployee(@RequestBody EmployeeDto employeeDto) {
		employeeeService.setEmployee(employeeDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/employee/{employeeid}")
	public ResponseEntity<Void> deleteStudent(@PathVariable(value = "employeeid") int empId) {
		employeeeService.deleteEmp(empId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	
	@PatchMapping(value = "/employee/{employeeid}")
	public ResponseEntity<Void> selUpdateEmp(@PathVariable(value = "employeeid") int empId,
				@RequestBody EmployeeDto employeeDto) {
		employeeeService.selUpdateEmp(empId, employeeDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}


	@PutMapping(value = "/employee/{employeeid}")
	public ResponseEntity<Void> updateEmp(@PathVariable(value = "employeeid") int empId,
				@RequestBody EmployeeDto employeeDto) {
		employeeeService.updateEmp(empId, employeeDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/employeead")
	public ResponseEntity<EmployeeDto[]> getEmployeesAdd() {
		return new ResponseEntity<EmployeeDto[]>(employeeeService.getEmployeesAdd(), HttpStatus.OK);

	}
	
	
}
