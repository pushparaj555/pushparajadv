//package com.example.demo.controller;
//
//import java.util.Map;
//import java.util.Set;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.demo.dto.MapDto;
//import com.example.demo.service.MapService;
//
//@RestController
//public class MapController {
//
//	@Autowired
//	@Qualifier(value = "mapServiceImpl")
//	private MapService mapservice;
//
//	@GetMapping (value="/map")
//	public ResponseEntity<Map<String, Map<String, Set<MapDto>>>> getmap(){
//		return new ResponseEntity <Map<String, Map<String, Set<MapDto>>>>(mapservice.getall(),HttpStatus.OK);
//		
//	}
//}
