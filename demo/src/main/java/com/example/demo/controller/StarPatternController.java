package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.StarPatternService;

@RestController
public class StarPatternController {

	@Autowired
	@Qualifier(value = "starPAtternServiceImpl") 
	private StarPatternService starPatternservice;

	@GetMapping(value = "/pattern")
	public ResponseEntity<String> printPattern(@RequestParam(value = "rows") int rows,
				@RequestParam(value = "reqform") String reqForm) {
		String pattern = starPatternservice.printPattern(rows, reqForm);
		return new ResponseEntity<>(pattern, HttpStatus.OK);
	}

	@GetMapping(value = "/ashpattern")
	public ResponseEntity<String> numberPattern(@RequestParam(value = "rows") int rows,
				@RequestParam(value = "reqform") String reqForm) {
		String pattern = starPatternservice.numberPattern(rows, reqForm);
		return new ResponseEntity<>(pattern, HttpStatus.OK);
	}

	@GetMapping(value = "/starRevpattern")
	public ResponseEntity<String> revPattern(@RequestParam(value = "rows") int rows,
				@RequestParam(value = "reqform") String reqForm) {
		String pattern = starPatternservice.revPattern(rows, reqForm);
		return new ResponseEntity<>(pattern, HttpStatus.OK);
	}
}
