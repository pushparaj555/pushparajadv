package com.example.demo.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CollectionDto;
import com.example.demo.dto.CollectionDto;
import com.example.demo.service.CollectionSortService;


@RestController
public class CollectionSortController {

	@Autowired
	@Qualifier(value = "collectionServiceImpls")
	private CollectionSortService collectionSortService;
	
	@GetMapping(value = "/sorting")
	public ResponseEntity<List<CollectionDto>> getAll(@RequestParam (value = "sortby") String sortBy) {
		ResponseEntity<List<CollectionDto>> responseEntity = new ResponseEntity<>(collectionSortService.getAll(sortBy), HttpStatus.OK);
		return responseEntity;
	}
}
