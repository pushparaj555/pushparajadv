package com.example.demo.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CollectionDto;
import com.example.demo.service.CollectionService;

@RestController
public class CollectionController {

	@Autowired
	@Qualifier(value = "collectionServiceImpl")
	private CollectionService service;

	@GetMapping(value = "/listGet")
	public ResponseEntity<List<CollectionDto>> listGet() {
		return new ResponseEntity<List<CollectionDto>>(service.listGet(), HttpStatus.OK);
	}

	@PostMapping(value = "/listSet")
	public ResponseEntity<Void> listSet(@RequestBody CollectionDto collectionDto) {
		service.listSet(collectionDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@GetMapping(value = "/setGet")
	public ResponseEntity<Set<CollectionDto>> setGet() {
		return new ResponseEntity<Set<CollectionDto>>(service.setGet(), HttpStatus.OK);
	}

	@PostMapping(value = "/setSet")
	public ResponseEntity<Void> setSet(@RequestBody CollectionDto collectionDto) {
		service.setSet(collectionDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

}
