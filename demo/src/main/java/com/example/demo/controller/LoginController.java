package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.LoginDto;
import com.example.demo.dto.RegisterDto;
import com.example.demo.service.LoginServices;

@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class LoginController {

	@Autowired

	@Qualifier(value = "loginServicesImpl")
	LoginServices service;

	@PostMapping(value = "/registoration")
	public ResponseEntity<Void> registerStudents(@RequestBody RegisterDto registerDto) {
		service.register(registerDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PostMapping(value = "/loginPage")
	public ResponseEntity<Boolean> verify(@RequestBody LoginDto renameDto) {
		return new ResponseEntity<Boolean>(service.verifyUser(renameDto), HttpStatus.OK);
	}

}
