package com.example.demo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CharacterDto;
import com.example.demo.service.StringService;

@RestController

@CrossOrigin(origins = "http://127.0.0.1:5500")
public class StringController {

	@Autowired
	@Qualifier(value = "stringService")
	private StringService stringService;

	@GetMapping(value = "string/count")
	public ResponseEntity<Integer>
				countVal(@RequestParam(value = "givenString") String givenString) {
		return new ResponseEntity<Integer>(stringService.countVal(givenString), HttpStatus.OK);
	}

	@GetMapping(value = "string/reverse")
	public ResponseEntity<String>
				reverseString(@RequestParam(value = "givenString") String givenString) {
		return new ResponseEntity<String>(stringService.reverseString(givenString), HttpStatus.OK);
	}

	@GetMapping(value = "string/repeat")
	public ResponseEntity<CharacterDto[]>
				ocuranceString(@RequestParam(value = "givenString") String givenString) {
		return new ResponseEntity<CharacterDto[]>(stringService.ocuranceString(givenString),
					HttpStatus.OK);
	}

}
