package com.example.demo.collection.sort;

import java.util.Comparator;

import com.example.demo.dto.CollectionDto;

public class IdComparator implements Comparator<CollectionDto> {

	@Override
	public int compare(CollectionDto o1, CollectionDto o2) {
		return o1.getId() - o2.getId();

	}

}
