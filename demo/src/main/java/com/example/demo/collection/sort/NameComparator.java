package com.example.demo.collection.sort;

import java.util.Comparator;


import com.example.demo.dto.CollectionDto;

public class NameComparator implements Comparator<CollectionDto>  {

	@Override
	public int compare(CollectionDto obj1, CollectionDto obj2) {
		return obj1.getName().compareTo(obj2.getName());
	}
}


