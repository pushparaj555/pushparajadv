package com.example.demo.collection.sort;

import java.util.Comparator;

import com.example.demo.dto.CollectionDto;

public class EmailComparator implements Comparator<CollectionDto> {

	@Override
	public int compare(CollectionDto object1, CollectionDto object2) {
		return object1.getEmail().compareTo(object2.getEmail());
	}
}
