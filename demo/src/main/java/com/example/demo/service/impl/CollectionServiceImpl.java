package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.example.demo.dto.CollectionDto;
import com.example.demo.service.CollectionService;

@Service(value = "collectionServiceImpl")
public class CollectionServiceImpl implements CollectionService {

	private List<CollectionDto> stringList = new ArrayList<>();

	private Set<CollectionDto> StringSet = new HashSet<CollectionDto>();

	@Override
	public void listSet(CollectionDto collectionDto) {
		stringList.add(collectionDto);
	}

	@Override
	public List<CollectionDto> listGet() {
		return stringList;
	}

	@Override
	public void setSet(CollectionDto collectionDto) {
		StringSet.add(collectionDto);

	}

	@Override
	public Set<CollectionDto> setGet() {
		return StringSet;
	}
}
