package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import com.example.demo.service.ArrayService;

@Service("arrayServiceImpl")
public class ArrayServiceImpl implements ArrayService {

	@Override
	public Integer[] array() {
		Integer arr[] = { 1, 2, 3, 4, 5 };
		Integer size = (arr.length);
		Integer k = 0;
		Integer arr2[] = new Integer[size];
		for (int i = 1; i <= arr.length; i++) {
			arr2[k++] = i;
		}
		return arr2;
	}

	@Override
	public Integer[] arrayEven(int val, int val2) {
		int size = (val2 - val) / 2;
		int k = 0;
		Integer evArr[] = new Integer[size];
		for (int i = val; i <= val2; i++) {
			if (i % 2 == 0) {
				evArr[k++] = i;
			}
		}
		return evArr;
	}

	@Override
	public int[] arrayOdd(int val, int val2) {
		int size = (val2 - val) / 2;
		int k = 0;
		int evArr[] = new int[size];
		for (int i = val; i <= val2; i++) {
			if (i % 2 != 0) {
				evArr[k++] = i;
			}
		}
		return evArr;
	}

	public int[] array1to100(int val, int val2) {
		int size = (val2 - val);
		int k = 0;
		int array[] = new int[size];
		for (int i = val; i < val2; i++) {

			array[k++] = i;
		}
		return array;
	}

	@Override
	public int[] sumArray() {
		int a[] = { 2, 3, 4, 5, 6, 3 };
		int b[] = { 4, 5, 6, 7, 6, 4 };
		int resultOdd = 0;
		int resultEven = 0;
		int d = 0;
		int c[] = new int[a.length];
		for (int i = 0; i < c.length; i++) {
			c[i] = a[i] + b[i];
		}
		for (int i = 0; i < c.length; i++) {
			if (c[i] % 2 == 0) {
				resultEven += c[i];
			} else {
				resultOdd += c[i];
			}
		}
		for (int k = 0; k < c.length; k++) {
			System.out.print(c[k] + " ");
		}
		return c;
	}

}
