package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.CollectionDto;


public interface CollectionSortService {

	public List<CollectionDto> getAll(String sortBy);
	
}



