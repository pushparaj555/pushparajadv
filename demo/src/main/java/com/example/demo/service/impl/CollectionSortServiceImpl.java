package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.collection.sort.EmailComparator;
import com.example.demo.collection.sort.IdComparator;
import com.example.demo.collection.sort.NameComparator;
import com.example.demo.dto.CollectionDto;
import com.example.demo.exception.SortNotSupportedException;
import com.example.demo.service.CollectionSortService;

@Service(value = "collectionServiceImpls")
public class CollectionSortServiceImpl implements CollectionSortService {
	

	@Override
	public List<CollectionDto> getAll(String sortBy) {
		List<CollectionDto> empDtos = new ArrayList<CollectionDto>();
		CollectionDto empDto1 = new CollectionDto(2, "Tiger", "Tiger@gmail.com");
		CollectionDto empDto2 = new CollectionDto(1, "Lion", "Lion@gmail.com");
		CollectionDto empDto3 = new CollectionDto(4, "Zebra", "Zebra@gmail.com");
		CollectionDto empDto4 = new CollectionDto(3, "Cow", "cow@gmail.com");
		empDtos.add(empDto1);
		empDtos.add(empDto2);
		empDtos.add(empDto3);
		empDtos.add(empDto4);
		
		if (sortBy != null) {
			if (sortBy.equalsIgnoreCase("name")) {
				Collections.sort(empDtos, new NameComparator());
			} else if (sortBy.equalsIgnoreCase("id")) {
				Collections.sort(empDtos, new IdComparator());
			}else if(sortBy.equalsIgnoreCase("email")){ 
				Collections.sort(empDtos, new EmailComparator());
			}
			else {
				throw new SortNotSupportedException("EC001", "This sorting is currently not supported");
			}
		}
		return empDtos;
	}
	
	
}
