package com.example.demo.service;

public interface StarPatternService {

	public String printPattern(int rows, String reqForm);

	public String numberPattern(int rows, String reqForm);
	
	public String revPattern(int rows, String reqForm);

	
	 
	
	
}
