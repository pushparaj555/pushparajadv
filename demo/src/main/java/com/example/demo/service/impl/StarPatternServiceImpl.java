package com.example.demo.service.impl;


import org.springframework.stereotype.Service;

import com.example.demo.service.StarPatternService;

@Service(value = "starPAtternServiceImpl")
public class StarPatternServiceImpl  implements StarPatternService {

	@Override
	public String printPattern(int rows, String reqForm) {
		String pattern = "";
		for (int i = 1; i < rows; i++) {
			for (int j = 1; j <= i; j++) {
				pattern += "*";
			}
			pattern += (reqForm != null && reqForm.equals("postman")) ? "\n" : "<br/>";
		}
		return pattern;
	}
	
	@Override
	public String numberPattern(int rows, String reqForm) {
		String pattern = "";
		for (int i = 1; i < rows; i++) {
			for (int j = 1; j <= i; j++) {
				pattern += "#";
			}
			pattern += (reqForm != null && reqForm.equals("postman")) ? "\n" : "<br/>";
		}
		return pattern;
	}
	
	
	@Override
	public String revPattern(int rows, String reqForm) {
		String pattern = "";
		for (int i = 1; i <= rows; i++) {
			for (int j = rows; j >= i; j--) {
				pattern += "*";
			}
			pattern += (reqForm != null && reqForm.equals("postman")) ? "\n" : "<br/>";
		}
		return pattern;
	}
	
	
		
	}
	
	


		





