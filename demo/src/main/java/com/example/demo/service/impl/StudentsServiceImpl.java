package com.example.demo.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.entity.AddressEntity;
import com.example.demo.entity.StudentEntity;
import com.example.demo.repository.StudentRepository;
import com.example.demo.dto.AddressDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.service.StudentService;

@Service(value = "studentServiceImpl")
public class StudentsServiceImpl implements StudentService {

	private Logger log = LogManager.getLogger(StudentsServiceImpl.class);

	@Autowired
	private StudentRepository studentRepository;

	public List<StudentDto> getStudents() {
		List<StudentEntity> studentEntities = studentRepository.findAll();
		Iterator<StudentEntity> studentEntityIter = studentEntities.iterator();
		List<StudentDto> studentDtos = new ArrayList<StudentDto>();
		while (studentEntityIter.hasNext()) {
			StudentEntity studentEntity = (StudentEntity) studentEntityIter.next();
			StudentDto studentDto = transformEntityToDto(studentEntity);
			studentDtos.add(studentDto);
		}
		return studentDtos;
	}

	private StudentDto transformEntityToDto(StudentEntity studentEntity) {
		StudentDto studentDto = new StudentDto();
		studentDto.setId(studentEntity.getId());
		studentDto.setName(studentEntity.getName());
		studentDto.setAge(studentEntity.getAge());
		studentDto.setClasss(studentEntity.getClasss());
		studentDto.setSection(studentEntity.getSection());
		List<AddressDto> addressDtos = new ArrayList<>();
		Set<AddressEntity> addressEntities = studentEntity.getAddressEntities();
		Iterator<AddressEntity> addressEntityIterator = addressEntities.iterator();
		while (addressEntityIterator.hasNext()) {
			AddressEntity addressEntity = addressEntityIterator.next();
			AddressDto addressDto = transformAddressEntityToDto(addressEntity);
			addressDtos.add(addressDto);
		}
		studentDto.setAddressDtos(addressDtos);
		return studentDto;
		
		
	}
	
	public StudentDto getParticularStudent(long studentId) {
		StudentEntity studentEntity = studentRepository.getById(studentId);
		StudentDto studentDto = transformEntityToDto(studentEntity);
		return studentDto;
	}
	
	public void createStudent(StudentDto studentDto) {
		StudentEntity studentEntity = transformDtoToEntity(studentDto);
		studentEntity.setId(0);
		studentRepository.saveAndFlush(studentEntity);
	}

	private StudentEntity transformDtoToEntity(StudentDto studentDto) {
		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setId(studentDto.getId());
		studentEntity.setName(studentDto.getName());
		studentEntity.setAge(studentDto.getAge());
		studentEntity.setClasss(studentDto.getClasss());
		studentEntity.setSection(studentDto.getSection());
		Set<AddressEntity> addressEntities = new HashSet<>();
		List<AddressDto> addressDtos = studentDto.getAddressDtos();
		Iterator<AddressDto> addressIterator = addressDtos.iterator();
		while (addressIterator.hasNext()) {
			AddressDto addressDto = addressIterator.next();
			AddressEntity addressEntity = transformAddressDtoToEntity(addressDto);
			addressEntities.add(addressEntity);
		}
		studentEntity.setAddressEntities(addressEntities);
		return studentEntity;
	}
	 
	private AddressDto transformAddressEntityToDto (AddressEntity addressEntity) {
		AddressDto addressDto = null;
		if (addressEntity != null) {
			addressDto = new AddressDto();
			addressDto.setId(addressEntity.getId());
			addressDto.setArea(addressEntity.getArea());
			addressDto.setDistrict(addressEntity.getDistrict());
			addressDto.setPincode(addressEntity.getPincode());
			
		}
		return addressDto;
	}
	
	
	private AddressEntity transformAddressDtoToEntity (AddressDto addressDto) {
		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setId(addressDto.getId());
		addressEntity.setArea(addressDto.getArea());
		addressEntity.setDistrict(addressDto.getDistrict());
		addressEntity.setPincode(addressDto.getPincode());
		return addressEntity;
	}
	
	
	
	public void updateStudent(long studentId, StudentDto studentDto) {
		StudentEntity studentEntity = transformDtoToEntity(studentDto);
		studentEntity.setId(studentId);
		studentRepository.saveAndFlush(studentEntity);
	}
	
	public void selUpdStudent(long studentId, StudentDto studentDto) {
		
		StudentDto studentDtoFromDB = getParticularStudent(studentId);
		StudentEntity studentEntity = transformDtoToEntity(studentDtoFromDB);
		if (studentDto.getAge() > 0) {
			studentEntity.setAge(studentDto.getAge());
		}
		if (studentDto.getName() != null) {
			studentEntity.setName(studentDto.getName());
		}
		if (studentDto.getSection() != null) {
			studentEntity.setSection(studentDto.getSection());
		}
		if (studentDto.getClasss() != null) {
			studentEntity.setClasss(studentDto.getClasss());
		}
		studentRepository.saveAndFlush(studentEntity);
	}
	
	
	
	public void deleteStudent(long studentId) {
		studentRepository.deleteById(studentId);
	}
}