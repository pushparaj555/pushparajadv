package com.example.demo.service;

import java.util.List;
import java.util.Set;

import com.example.demo.dto.CollectionDto;

public interface CollectionService {

	public List<CollectionDto> listGet();

	public void listSet(CollectionDto collectionDto);

	public Set<CollectionDto> setGet();

	public void setSet(CollectionDto collectionDto);
}
