package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.EmployeeDto;

public interface EmployeeServiceList {

	public List<EmployeeDto> getEmployees();

	public EmployeeDto getEmp(int empId);

	public void createEmp(EmployeeDto empDto);

	public void deleteEmp(int empId);

	public void selUpdateEmp(int empId, EmployeeDto employeeDto);

	public void updateEmp(int empId, EmployeeDto employeeDto);

	public List<EmployeeDto> getEmployeesAdd();

	public void setEmployee(EmployeeDto employeeDto);
}
