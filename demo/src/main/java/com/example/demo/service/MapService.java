package com.example.demo.service;

import java.util.Map;
import java.util.Set;

import com.example.demo.dto.MapDto;

public interface MapService {

//	public List<MapDto> getAll();
	
	Map<String, Map<String, Set<MapDto>>> getall();
}
