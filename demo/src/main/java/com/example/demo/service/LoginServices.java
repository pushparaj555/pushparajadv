package com.example.demo.service;

import com.example.demo.dto.LoginDto;
import com.example.demo.dto.RegisterDto;

public interface LoginServices {

	public void register(RegisterDto regDto);

	public boolean verifyUser(LoginDto lDto);

}
