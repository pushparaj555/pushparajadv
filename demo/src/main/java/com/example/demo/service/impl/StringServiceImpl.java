package com.example.demo.service.impl;

import org.springframework.stereotype.Service;

import com.example.demo.dto.CharacterDto;
import com.example.demo.service.StringService;

@Service(value = "stringService")
public class StringServiceImpl implements StringService {

	public String reverseString(String givenString) {
		char[] name = givenString.toCharArray();
		char[] rev = new char[name.length];
		int j = 0;
		for (int i = name.length - 1; i >= 0; i--) {
			rev[j++] = name[i];
		}
		String revString = new String(rev);
		return revString;
	}

	public Integer countVal(String givenString) {
		char[] name = givenString.toCharArray();
		Integer res = name.length;
		return res;
	}

	public CharacterDto[] ocuranceString(String givenString) {
		char chars[] = givenString.toCharArray();
		CharacterDto[] charDto = new CharacterDto[chars.length];

		char dup[] = new char[chars.length];
		int count[] = new int[chars.length];
		int lastIndex = 0;
		for (int i = 0; i < chars.length; i++) {
			boolean isDup = false;
			int index = 0;
			for (int j = 0; j < dup.length; j++) {
				if (chars[i] == dup[j]) {
					index = j;
					isDup = true;
					break;
				}

			}
			if (isDup) {
				count[index]++;
			} else {
				dup[lastIndex] = chars[i];
				count[lastIndex++]++;
			}
		}

		for (int i = 0; i < count.length; i++) {
			CharacterDto dto = new CharacterDto();
			dto.setVal(dup[i]);
			dto.setCount(count[i]);
			charDto[i] = dto;
		}

		return charDto;
	}

}
