package com.example.demo.service;

import com.example.demo.dto.CharacterDto;

public interface StringService {

//	public Integer countVal(String givenString);
	public String reverseString(String givenString);

	public Integer countVal(String givenString);

	public CharacterDto[] ocuranceString(String givenString);

}
