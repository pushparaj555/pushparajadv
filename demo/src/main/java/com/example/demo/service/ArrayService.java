package com.example.demo.service;

public interface ArrayService {
	
	

	public Integer[] array();

	public Integer[] arrayEven(int val, int val2);

	public int[] arrayOdd(int val, int val2);
	
	public int[]array1to100(int val , int val2);
	
	public int[] sumArray();
}
