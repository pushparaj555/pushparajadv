package com.example.demo.service;

import com.example.demo.dto.EmpAddressDto;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.StudentDto;

public interface EmployeeService {

	public EmployeeDto[] getEmployees();

	public EmployeeDto getEmp(int empId);

	public void createEmp(EmployeeDto empDto);

	public void deleteEmp(int empId);

	public void selUpdateEmp(int empId, EmployeeDto employeeDto);

	public void updateEmp(int empId, EmployeeDto employeeDto);

	public EmployeeDto[] getEmployeesAdd();

	public void setEmployee(EmployeeDto employeeDto);
}
