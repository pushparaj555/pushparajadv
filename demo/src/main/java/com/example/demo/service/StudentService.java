package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.StudentDto;

public interface StudentService {

	public List<StudentDto> getStudents();

	public StudentDto getParticularStudent(long studentId);

	public void createStudent(StudentDto studentDto);

	public void updateStudent(long studentId, StudentDto studentDto);

	public void selUpdStudent(long studentId, StudentDto studentDto);

	public void deleteStudent(long studentId);


	

}
