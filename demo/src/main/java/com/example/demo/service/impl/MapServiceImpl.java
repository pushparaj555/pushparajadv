package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.example.demo.dto.MapDto;
import com.example.demo.service.MapService;
@Service (value = "mapServiceImpl")
public class MapServiceImpl implements MapService {

	@Override
	public Map<String, Map<String, Set<MapDto>>> getall() {
		MapDto obj1 = new MapDto(1, "pushparaj", 23, "11th", "A");
		MapDto obj2 = new MapDto(2, "raj", 22, "12th", "A");
		MapDto obj3 = new MapDto(3, "lion", 23, "11th", "A");
		MapDto obj4 = new MapDto(4, "tiger", 20, "10th", "A");
		MapDto obj5 = new MapDto(5, "zebra", 19, "9th", "B");
		MapDto obj6 = new MapDto(6, "cow", 23, "10th", "c");
		MapDto obj7 = new MapDto(7, "fox", 13, "8th", "D");
		MapDto obj8 = new MapDto(8, "raj", 23, "11th", "A");
		MapDto obj9 = new MapDto(9, "pushparaj", 23, "11th", "A");
		MapDto obj10 = new MapDto(10, "pushparaj", 23, "11th", "A");
		MapDto obj11 = new MapDto(11, "ant", 3, "1st", "A");
		MapDto obj12 = new MapDto(12, "mosquito", 23, "9th", "A");
		MapDto obj13 = new MapDto(13, "dog", 21, "10th", "A");
		List<MapDto> list = new ArrayList<>();
		list.add(obj1);
		list.add(obj2);
		list.add(obj3);
		list.add(obj4);
		list.add(obj5);
		list.add(obj6);
		list.add(obj7);
		list.add(obj8);
		list.add(obj9);
		list.add(obj10);
		list.add(obj11);
		list.add(obj12);
		list.add(obj13);

		Set<MapDto> mydtos = new TreeSet<>();
		mydtos.addAll(list);

		Map<String, Map<String, Set<MapDto>>> map1 = new HashMap<>();
		Iterator<MapDto> val = mydtos.iterator();
		while (val.hasNext()) {
			MapDto mydto = (MapDto) val.next();
			String standard = mydto.getStandard();
			String section = mydto.getSection();
			System.out.println(standard);
			System.out.println(section);
			if (map1.containsKey(standard)) {
				Map<String, Set<MapDto>> mapInner = map1.get(standard);
				if (mapInner.containsKey(section)) {
					Set<MapDto> newSet = mapInner.get(section);
					newSet.add(mydto);
				} else {
					Set<MapDto> newSet2 = new TreeSet<MapDto>();
					newSet2.add(mydto);
					mapInner.put(section, newSet2);
				}
			} else {
				Set<MapDto> finalSet = new TreeSet<>();
				Map<String, Set<MapDto>> finalMap = new HashMap<>();
				finalSet.add(mydto);
				finalMap.put(section, finalSet);
				map1.put(standard, finalMap);

			}
		}

		System.out.println(map1);

		return map1;
	}

}
