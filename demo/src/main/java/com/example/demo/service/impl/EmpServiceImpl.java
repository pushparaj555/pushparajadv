package com.example.demo.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.stereotype.Service;

import com.example.demo.dto.AddressDto;
import com.example.demo.dto.EmpAddressDto;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.StudentDto;
import com.example.demo.service.EmployeeService;

@Service(value = "empServiceImpl")
public class EmpServiceImpl implements EmployeeService {
	private Logger log = LogManager.getLogger(EmpServiceImpl.class);

	public EmployeeDto[] getEmployees() {
		EmployeeDto[] employeeDto = new EmployeeDto[100];
		try {
			Connection connection = getConnectionObj();
			Statement stmt = connection.createStatement();
			ResultSet std = stmt.executeQuery("select emp.id,emp.name,emp.age,emp.email from emp");
			int i = 0;
			while (std.next()) {
				int id = std.getInt(1);
				String name = std.getString(2);
				int age = std.getInt(3);
				String email = std.getString(4);
				EmployeeDto employeeDt = createEmployeeDtom(id, name, age, email);
				employeeDto[i++] = employeeDt;
			}
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
		return employeeDto;
	}
	
	public EmployeeDto[] getEmployeesAdd() {
		EmployeeDto[] employeeDto = new EmployeeDto[100];
		try {
			Connection connection = getConnectionObj();
			Statement stmt = connection.createStatement();
			ResultSet std = stmt.executeQuery("select emp.id, emp.name,emp.age,emp.email,addres.area,addres.pincode from addres inner join emp on addres.emp_id = emp.id;");
			int i = 0;
			while (std.next()) {
				int id = std.getInt(1);
				String name = std.getString(2);
				int age = std.getInt(3);
				String email = std.getString(4);
				
				String area = std.getString(5);
				int pincode = std.getInt(6);
				
				EmpAddressDto addDto = new EmpAddressDto();
				addDto.setArea(area);
				addDto.setPincode(pincode);
				EmployeeDto employeeDt = createEmployeeDto(id, name, age, email,addDto);
				employeeDto[i++] = employeeDt;
			}
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
		return employeeDto;
	}
	
	
	public void setEmployee (EmployeeDto employeeDto) {
		try {

			Connection connection = getConnectionObj();
			PreparedStatement pStmt = connection
						.prepareStatement("insert into emp (name, age, email) values (?, ?, ?)");
			pStmt.setString(1, employeeDto.getName());
			pStmt.setInt(2, employeeDto.getAge());
			pStmt.setString(3, employeeDto.getEmail());
			
//			ResultSet res = pStmt.executeQuery("select max(id)from emp");

//			int id = 0;
//			while (res.next()) {
//				id = res.getInt(1);
//			}

			PreparedStatement pStmt1 = connection
						.prepareStatement("insert into addres (area,pincode,emp_id) values( ?, ?, ?)");
			EmpAddressDto addressDto = employeeDto.getAddress();
			pStmt1.setString(1, addressDto.getArea());
			pStmt1.setInt(2, addressDto.getPincode());
			pStmt1.setInt(3, addressDto.getEmp_id());
			pStmt.execute();
			pStmt1.execute();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();

		}
	}
	
	
	
	
	public void createEmp(@RequestBody EmployeeDto employeeDto) {
		try {
			Connection connection = getConnectionObj();
			// Create Statement
			PreparedStatement pStmt = connection
						.prepareStatement("insert into emp (name, age, email) values (?, ?, ?)");

			pStmt.setString(1, employeeDto.getName());
			pStmt.setInt(2, employeeDto.getAge());
			pStmt.setString(3, employeeDto.getEmail());
			

			boolean insertStatus = pStmt.execute();
//			log.info("Student Creation Status = " + insertStatus);
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
//		log.info("studentDto " + studentDto);
	}

	
	
	
	
	
	
	
	
	
	

	private EmployeeDto createEmployeeDto(int id, String name, int age, String email,EmpAddressDto addDto) {
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setId(id);
		employeeDto.setName(name);
		employeeDto.setAge(age);
		employeeDto.setEmail(email);
		employeeDto.setAddress(addDto);

		return employeeDto;

	}
	private EmployeeDto createEmployeeDtom(int id, String name, int age, String email) {
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setId(id);
		employeeDto.setName(name);
		employeeDto.setAge(age);
		employeeDto.setEmail(email);
		

		return employeeDto;

	}

	private Connection getConnectionObj() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/employee", "root", "jillaraj@5");
		return connection;
	}
	
	
	@Override
	public void deleteEmp(int empId) {
		try {
			Connection connection = getConnectionObj();
			EmployeeDto empFromDb = getEmp(empId);
			PreparedStatement std = connection.prepareStatement("DELETE FROM emp where id = "
						+ empFromDb.getId());
			std.execute();

		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	
	
	public void updateEmp(int empId, EmployeeDto employeeDto) {
		try {
			Connection connection = getConnectionObj();
			// Create Statement
			PreparedStatement pStmt = connection
						.prepareStatement("update emp set name=?,age=?, email=? where id=?");
			pStmt.setString(1, employeeDto.getName());
			pStmt.setInt(2, employeeDto.getAge());
			pStmt.setString(3, employeeDto.getEmail());
			
			pStmt.setInt(4, empId);

			boolean updateStatus = pStmt.execute();
//			log.info("Student Creation Status = " + updateStatus);
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
//		log.info("StudentDTO updated successfully ---------");
	}
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void selUpdateEmp(int empId, EmployeeDto employeeDto) {
		try {
			Connection connection = getConnectionObj();
			EmployeeDto empDtoFromDb = empNullException(empId, employeeDto);

			PreparedStatement pStmt = connection
						.prepareStatement("update emp set name=?,age=?, email=? where id=?");
			pStmt.setString(1, empDtoFromDb.getName());
			pStmt.setInt(2, empDtoFromDb.getAge());
			pStmt.setString(3, empDtoFromDb.getEmail());
			
			pStmt.setInt(4, empDtoFromDb.getId());

			boolean updateStatus = pStmt.execute();
//			log.info("Student Creation Status = " + updateStatus);
		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	private EmployeeDto empNullException(int empId, EmployeeDto employeeDto) {
		EmployeeDto empDtoFromDb = getEmp(empId);
		if (employeeDto.getName() != null) {
			empDtoFromDb.setName(employeeDto.getName());
		}
		if (employeeDto.getAge() != 0) {
			empDtoFromDb.setAge(employeeDto.getAge());
		}
		if (employeeDto.getClass() != null) {
			empDtoFromDb.setEmail(employeeDto.getEmail());
		}
		
		
		return empDtoFromDb;
	}
	
	

	

	
	
	
	@Override
	public EmployeeDto getEmp(int empId) {
		EmployeeDto employeeDto = null;
		try {
			Connection connection = getConnectionObj();
			Statement stmt = connection.createStatement();
			ResultSet std = stmt
						.executeQuery("select emp.id,emp.name,emp.age,emp.email from emp where emp.id="
									+ empId);
								
			while (std.next()) {
				int id = std.getInt(1);
				String name = std.getString(2);
				int age = std.getInt(3);
				String email = std.getString(4);
				

				employeeDto = createEmployeeDtom(id, name, age, email);
			}

		} catch (ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
//		log.info("studentDto " + studentDto);
		return employeeDto;
	}	
	
	
	
	
	
	
	
	
	
	
	
	

}
