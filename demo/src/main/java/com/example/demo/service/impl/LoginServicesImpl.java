package com.example.demo.service.impl;

import java.lang.System.Logger;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.LoginDto;
import com.example.demo.dto.RegisterDto;
import com.example.demo.dto.RegisterDto;
import com.example.demo.entity.LoginEntity;
import com.example.demo.entity.RegisterEntity;
import com.example.demo.repository.LoginRepository;
import com.example.demo.repository.RegisterRepository;
import com.example.demo.service.LoginServices;

@Service(value = "loginServicesImpl")
public class LoginServicesImpl implements LoginServices {
	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private RegisterRepository registerRepository;

	

	@Override
	public void register(RegisterDto registerDto) {
		LoginEntity loginEntity = loginDtoToEntity(registerDto);
		if (loginEntity != null) {
			loginRepository.save(loginEntity);
		}
		RegisterEntity registerEntity = regDtoTOEntity(registerDto);
		if (registerEntity != null) {
			registerRepository.save(registerEntity);
		}
	}

	public LoginEntity loginDtoToEntity(RegisterDto regisDto) {
		LoginEntity loginEntity = new LoginEntity();
		LoginDto loginDto = regisDto.getLoginDto();
		
		loginEntity.setUsername(loginDto.getUsername());
		loginEntity.setPassword(loginDto.getPassword());
		return loginEntity;
	}

	public RegisterEntity regDtoTOEntity(RegisterDto registerDto) {
		RegisterEntity entity = new RegisterEntity();
		entity.setEmail(registerDto.getEmail());
		entity.setFirstName(registerDto.getFirstName());
		entity.setLastName(registerDto.getLastName());
		return entity;
	}

	@Override
	public boolean verifyUser(LoginDto loginDto) {
		boolean isValid = false;
		LoginEntity loginEntity = loginRepository.findByIsDeletedAndUsernameAndPassword(false, loginDto
					.getUsername(), loginDto.getPassword());
		if (loginEntity != null) {
			isValid = true;
		}
		
		return isValid;
	}

}