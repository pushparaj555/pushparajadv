package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.dto.ErrorDto;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ SortNotSupportedException.class })
	public ResponseEntity<ErrorDto> handleSortNotSupportedEx(SortNotSupportedException iEx) {
		String errorMessage = iEx.getErrorMessage();
		String errorCode = iEx.getErrorCode();
		ErrorDto errorMessageDTO = new ErrorDto(errorCode, errorMessage);
		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorMessageDTO,
					HttpStatus.BAD_REQUEST);
		return responseEntity;
	}

//	@ExceptionHandler({ InvalidInputException.class })
//	public ResponseEntity<ErrorDto> handleInvalidInputEx(InvalidInputException iEx) {
//		String errorMessage = iEx.getErrorMessage();
//		String errorCode = iEx.getErrorCode();
//		ErrorDto errorMessageDTO = new ErrorDto(errorCode, errorMessage);
//		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorMessageDTO,
//					HttpStatus.BAD_REQUEST);
//		return responseEntity;
//	}

//	@ExceptionHandler({ Exception.class })
//	public ResponseEntity<ErrorDto> handleInvalidInputEx(Exception iEx) {
//		String errorMessage = iEx.getMessage();
//		String errorCode = "EC1001";
//		ErrorDto errorMessageDTO = new ErrorDto(errorCode, errorMessage);
//		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorMessageDTO,
//					HttpStatus.INTERNAL_SERVER_ERROR);
//		return responseEntity;
//	}
}
