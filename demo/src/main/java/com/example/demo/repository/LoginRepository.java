package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.LoginEntity;
import com.example.demo.entity.RegisterEntity;

@Repository
public interface LoginRepository extends JpaRepository<LoginEntity, Integer> {

	LoginEntity findByIsDeletedAndUsernameAndPassword(boolean isDeleted, String username,
				String password);


}