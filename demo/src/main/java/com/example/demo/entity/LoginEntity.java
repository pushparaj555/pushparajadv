package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "loginusers")
public class LoginEntity {

	@Id
	private int id;
	
	
	
	public int getId() {
		return id;
	}

	
	public void setId(int id) {
		this.id = id;
	}

	
	public boolean isDeleted() {
		return isDeleted;
	}

	
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column
	private String username;

	@Column
	private String password;

	@Column(name = "is_deleted")
	private boolean isDeleted;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}