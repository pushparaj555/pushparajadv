package com.example.demo.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "student")

public class StudentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	
	@Column(name = "age")
	private int age;
	
	@Column(name = "classs")
	private String classs;
	
	@Column(name = "section")
	private String section;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name = "stud_id", referencedColumnName = "id")
	
	private Set<AddressEntity> addressEntities;
	
	

	
	public Set<AddressEntity> getAddressEntities() {
		return addressEntities;
	}

	
	public void setAddressEntities(Set<AddressEntity> addressEntities) {
		this.addressEntities = addressEntities;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getClasss() {
		return classs;
	}

	
	public int getAge() {
		return age;
	}

	
	public void setAge(int age) {
		this.age = age;
	}

	
	public void setClasss(String classs) {
		this.classs = classs;
	}
}
