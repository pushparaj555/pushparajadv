package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")

public class RegisterEntity {

	@Id
	private long id;

	@Column(name = "email")
	private String email;

	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "is_deleted")
	private boolean isDeleted;

	public long getId() {
		return id;
	}

	
	public boolean isDeleted() {
		return isDeleted;
	}

	
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "lastName")
	private String lastName;

	

}
