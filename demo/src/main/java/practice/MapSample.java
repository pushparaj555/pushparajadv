package practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapSample {

	public static void main(String[] args) {
		List<String> str1 = new ArrayList<>();
		str1.add("lion");
		str1.add("lion");
		str1.add("lion");
		str1.add("cow");
		str1.add("fox");
		str1.add("cow");
		str1.add("tiger");
		str1.add("tiger");
		str1.add("lion");

		System.out.println(str1);

		Map<String, Integer> map = new HashMap<>();

		Iterator<String> iter = str1.iterator();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			if (map.containsKey(key)) {
				int val = map.get(key);
				map.put(key, val + 1);
			} else {
				map.put(key, 1);
			}

		}
		System.out.println(map);

	}

}
