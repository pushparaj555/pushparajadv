package practice;

import java.util.Iterator;

public class LinkedList {

	public static void main(String[] args) {
		java.util.LinkedList<String> link = new java.util.LinkedList<>();
		link.add("raj");
		link.add("raj");
		link.add("chandru");
		link.add("pushparaj");
		link.add("ajay");
		Iterator<String> itr = link.iterator();
		while (itr.hasNext()) {
			String val = (String) itr.next();
			System.out.println(val);
		}
	}
}