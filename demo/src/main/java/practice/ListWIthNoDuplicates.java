package practice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListWIthNoDuplicates {

	public static void main(String[] args) {
		List<String> str1 = new ArrayList<>();
		str1.add("lion");
		str1.add("lion");
		str1.add("lion");
		str1.add("tiger");
		str1.add("cat");
		str1.add("cat");
		str1.add("zebra");
		List<String> str2 = new ArrayList<>();
		for (int i = 0; i < str1.size(); i++) {
			String value = str1.get(i);
			if (!str2.contains(value)) {
				str2.add(value);
			}
		}
		System.out.println(str2);

		Set<String> names = new HashSet<>();
		names.addAll(str1);
		List<String> noDup = new ArrayList<>();
		noDup.addAll(names);
		System.out.println(noDup);
		
		
	}
}
