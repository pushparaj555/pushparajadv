package practice;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetModel2 {

	public static void main(String[] args) {
		Set<String> animals = new HashSet<>();
		animals.add("kangaroo");
		animals.add("lion");
		animals.add("tiger");
		animals.add("cheatah");
		animals.add("crocodile");
		animals.add("lion");
		animals.add("lion");
		System.out.println(animals);

		SortedSet<String> animal2 = new TreeSet<>();
		animal2.addAll(animals);
		System.out.println(animal2);

		Set<String> animal3 = new TreeSet<>();
		animal3.addAll(animal2);
		System.out.println(animal3);

	}
}
