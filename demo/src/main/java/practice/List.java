package practice;

import java.util.ArrayList;
import java.util.Iterator;

public class List {

	public static void main(String[] args) {
		java.util.List<String> list = new ArrayList<>();
		list.add("raj");
		list.add("pushparaj");
		list.add("pushparaj");
		list.add("ajay");
		list.add("chandru");
		list.add("balaji");
		Iterator<String> iter = list.iterator();
		while (iter.hasNext()) {
			String value = (String) iter.next();
			System.out.println(value);
		}
	}
}
