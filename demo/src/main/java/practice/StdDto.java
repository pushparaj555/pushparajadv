package practice;

public class StdDto implements Comparable<StdDto> {

	private int id;

	private String name;

	private int age;

	private String email;

	private String Standard;

	private String section;

	public StdDto(int id, String name, int age, String email, String standard, String section) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.email = email;
		Standard = standard;
		this.section = section;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStandard() {
		return Standard;
	}

	public void setStandard(String standard) {
		Standard = standard;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Override
	public String toString() {
		return "StdDto [id=" + id + ", name=" + name + ", age=" + age + ", email=" + email
					+ ", Standard=" + Standard + ", section=" + section + "] \n";
	}

	@Override
	public int compareTo(StdDto o) {
		return name.compareTo(o.getName());
	}

	@Override
	public boolean equals(Object obj) {
		StdDto dto = (StdDto) obj;
		return super.equals(dto.getName());
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
