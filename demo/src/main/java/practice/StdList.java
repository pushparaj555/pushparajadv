package practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.boot.SpringApplication;

public class StdList {

	public static void main(String[] args) {
		List<StdDto> list = new ArrayList<>();
		StdDto obj1 = new StdDto(1, "pushparaj", 23, "raj@gmail.com", "12", "a");
		StdDto obj2 = new StdDto(2, "pushparaj", 23, "raj@gmail.com", "12", "a");
		StdDto obj3 = new StdDto(3, "raj", 22, "raj33@gmail.com", "12", "a2");
		StdDto obj4 = new StdDto(4, "ajay", 22, "aj@gmail.com", "12", "a");
		StdDto obj5 = new StdDto(5, "balaji", 23, "bal@gmail.com", "11", "b");
		StdDto obj6 = new StdDto(6, "chandru", 21, "chan@gmail.com", "10", "c");

		list.add(obj1);
		list.add(obj2);
		list.add(obj3);
		list.add(obj4);
		list.add(obj5);
		list.add(obj6);
		Set<StdDto> stdSet = new TreeSet<>();
		stdSet.addAll(list);

		Map<String, Set<StdDto>> mpset = new Hashtable<>();
		Iterator<StdDto> iterator = stdSet.iterator();
		while (iterator.hasNext()) {
			StdDto stdDto = (StdDto) iterator.next();
			String std = stdDto.getStandard();
			if (mpset.containsKey(std)) {
				Set<StdDto> newSet = mpset.get(std);
				newSet.add(stdDto);
			} else {
				Set<StdDto> newSet2 = new TreeSet<StdDto>();
				newSet2.add(stdDto);
				mpset.put(std, newSet2);

			}

		}
		System.out.println(mpset);

	}
}
