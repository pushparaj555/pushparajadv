package patt;

import org.springframework.boot.SpringApplication;

public class RightMountain {

	public static void main(String[] args) {
		int size = 5;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size - i; j++) {
				System.out.print(" ");
			}
			for (int k = 0; k < i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
		int size1 = 5;
		for (int i = size1; i > 0; i--) {
			for (int j = 0; j < size - i; j++) {
				System.out.print(" ");
			}
			for (int k = 0; k < i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
