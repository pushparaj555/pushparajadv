package patt;

public class Constructor {

	public Constructor() {
		System.out.println("new constructor is created 1");
	}

	public static void main(String[] args) {
		Constructor b = new Constructor();
//		System.out.println(b);

		Constructor c = new Constructor();
//		System.out.println(c);

		Constructor d = new Constructor();
		d.meth1();
		
		Constructor val = new Constructor(19);

	}

	public void meth1() {
		System.out.println("i am inside the method");
	}

	public Constructor(int x) {
		this();
		System.out.println(x);
	}
}
